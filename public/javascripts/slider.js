document.addEventListener('DOMContentLoaded', function() {
  
	var current_element = 0
	var elements = document.querySelectorAll('.slider-content')
	var prev = document.querySelector('.prev')
	var next = document.querySelector('.next')

	function reDisplay(element_id = 0) {
		elements.forEach((element, index) => {
			if (element_id !== index) 
				return element.style.display = 'none'

			return element.style.display = 'block'
	    })
	}

	reDisplay(0)

	prev.addEventListener('click', function(event) {
		event.preventDefault()

		if (elements[current_element - 1])
			current_element -= 1

		reDisplay(current_element)
	})

	next.addEventListener('click', function(event) {
		event.preventDefault()
		
		if (elements[current_element + 1])
			current_element += 1

		reDisplay(current_element)
	})
})




